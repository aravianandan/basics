#include <iostream>
using namespace std;

int main()
{
	// if statement
	//cout << "Please enter a number: "; // string literal
	//int num = 0; // 0 => literal
	//cin >> num;
	//if (num < 0)
	//{
	//	num = 0 - num;
	//}
	///* odd / even
	//int rem = num % 2;
	//if (rem > 0) {
	//	std::cout << "num is odd\n";
	//}
	//else {
	//	std::cout << "num is even\n";
	//}
	//cout << "your number is: " << num << endl;
	//*/
	//// div by 2 or by 5
	//int rem2 = num % 2;
	//int rem5 = num % 5;
	//bool divby2n5 = 0 == rem2 && 0 == rem5;
	//if(divby2n5)
	//{
	//	cout << "Number is divisible by 2 and 5";
	//}
	//else if(0 == rem2)
	//{
	//	cout << "Number is divisible by 2";
	//}
	//else if (0 == rem5)
	//{
	//	cout << "Number is divisible by 5";
	//}
	//else
	//{
	//	cout << "Number is NOT divisible by 2/5";
	//}

	//int a = 0, b = 0, c = 0;
	//cout << "enter num 1: ";
	//cin >> a;
	//cout << "enter num 2: ";
	//cin >> b;
	//cout << "enter num 3: ";
	//cin >> c;
	//int max = (a > b) ? (a > c) ? a : c : b; // nested ternary operator
	//int min = (a < b) ? (a < c) ? a : c : b;
	//cout << "max is: " << max << "\n min is: " << min << std::endl;
	//looping 
	int count = 0;
	//while (count > 0)
	//{
	//	cout << "hello world\n";
	//	count = count - 1;
	//}
	//do
	//{
	//	cout << "hello world\n";
	//	count = count - 1;
	//} while (count > 0);
	for (int i = 0; i < 5; i++)
	{
		cout << i+1 << "(s)t(h) time: Hello world\n" << endl;
	}
	// arrays
	const int size = 5;
	int a[size]; //<- array of int; can hold 5 integers
	for (int i = 0; i < size; i++)
	{
		cout << "enter number\n";
		cin >> a[i];
	}
	cout << "user entered: \n";
	for (int i = 0; i < size; i++)
	{
		cout << a[i] << "\n";
	}
	int sum = 0;
	for (int i = 0; i < size; i++)
	{
		//sum = ? ;
	}

	return 0;
}