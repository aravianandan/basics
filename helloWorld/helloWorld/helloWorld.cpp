#include <iostream>
using namespace std;

int dummy()
{
    // data types
    bool success = true; // bool can store true/false
    //00000000; 11111111; 0010111011;
    /* 2^8 =  0-255 = 0|0000000
                      ^
                      |
                      ----- sign bit
    0000
    0001
    0010
    0011
    0100
    0101
    0110
    0111
    1000
    1001
    1010
    1011
    1100
    1101
    1110
    1111

    523 = 5 *(10^2) + 2 *(10^1) + 3 * (10^0)
    1011 = 1 * (2^3) + 0 * (2^2) + 1 * (2^1) + 1*(2^0) = 8 + 0 + 4 + 1 = 13
    100001011 = 523 = 1 * 2^9

    */
    char a = 'a'; // 1 byte long (ASCII encoding)
    unsigned char x = 255; // 0 - 255 
    //int 4/8 bytes = unsigned/signed
    unsigned short int si = 65535; // 0 - 65535
    //float / double = float => decimal numbers
    // double = high precision decimal numbers
    // xxxxxxx | xxxxxxxxxxxxxxxx -> exponent
    // |______|
    //    ^
    //    |
    //    ______________ mantisaa
    float f1 = 2.141;
    int len = strlen("hello world");
    double f2 = 2.1451345125123512512;
    cout << "size of int is " <<sizeof(int) << endl
		<< "size of char is " << sizeof(char)<< endl
		<< "size of int is " << sizeof(int)<< endl
		<< "size of long is " << sizeof(long long)<< endl
        << endl;

    int num1 = 0, num2 = 0;
    long long sum = 0;
    cout << "Please enter number: ";
    cin >> num1;
    cout << "Please enter another number: ";
    cin >> num2;
    sum = num1 + num2;
    // operators = (+,-,*,/,%,^)
    cout << "The sum is: " << sum << endl;
    //branching and looping 
    return 0;
}